# Cleandev Docs Libs
Este proyecto reune toda la relación entre todas las librerias del [gitlab](https://gitlab.com/cleansoftware/libs/public) de Cleandev.

Soy consciente de que pese a ser tedioso el mantener dichos diagramas pero es un granito de arena para
tomar buenas decisiones en cuanto a cambios y muchas otras ventajas.

A continuación se mostrara un plano general de las librerias y sus propias dependencias, el detalle de la implementación
diagrama interno de clases de cada librería esta contenplado dentro de cada librería.

Siempre se hara una diferencia entre las librerias categorizandolas en tres grupos
- Librerías Internas
- Librerías Externas
- Dependencias externas


### Librerías internas
Son todas aquellas que esta bajo este [repositorio](https://gitlab.com/cleansoftware/libs/public) que estan sujetas a cambios 
mientras no se haga oficial versiones estables de las mismas.


### Librerías externas

Son todas aquellas librerías que no estan creadas por por "Cleandev" y por lo tanto queda fuera de control posibles errores
o mantenimientos a futuro, no obtante se trara de buscar soluciones en las librerías propietarias si esto fuese un inconveniente


### Dependencias Externas

Son aquellas librerias que dependen de las librerias externas. En la mayoria de casos no sabemos exactamente cuales son sus
relaciones con dichas librerias por lo que las representamos unicamente con un paquete externo sin relación ya que no necesitamos
saber su relación pero si nos interesa tener conocimiento que dependemos de ellas de algun modo


----
- cleandev-generic-utils (0.1.6)
  - [dependencia de librerías](diagramas/cleandev-generic-utils)
  - [diagrama de clases](https://gitlab.com/cleansoftware/libs/public/cleandev-generic-utils)
- cleandev-config-loader (0.3.1)
  - [dependencia de librerías](diagramas/cleandev-config-loader)
  - [diagrama de clases](https://gitlab.com/cleansoftware/libs/public/cleandev-config-loader)
- cleandev-postgresql-db (0.3.3)
  - [dependencia de librerías](diagramas/cleandev-postgresql-db)
  - [diagrama de clases](https://gitlab.com/cleansoftware/libs/public/cleandev-postgresql-db)